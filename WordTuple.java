import java.util.Comparator;

/**
 * Created by linux on 11.12.16.
 */

class WordTupleComparator implements Comparator<WordTuple<String, Integer>> {
    @Override
    public int compare(WordTuple<String, Integer> t1, WordTuple<String, Integer> t2) {
        int count_cmp = t2.count.compareTo(t1.count);

        if (count_cmp == 0)
            return t2.word.compareTo(t1.word);

        return count_cmp;
    }
}

public class WordTuple<X, Y> {
    public X word;
    public Y count;

    public WordTuple(X x, Y y) {
        this.word = x;
        this.count = y;
    }

    public static WordTupleComparator comparator()
    {
        return new WordTupleComparator();

    }
}
