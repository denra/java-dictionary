import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by linux on 21.12.16.
 */
public class TreeSetDictionary implements IDictionary {
    TreeSet<WordTuple<String, Integer>> treeSet;

    TreeSetDictionary()
    {
        Comparator comparator = new Comparator<WordTuple<String, Integer>>() {
            @Override
            public int compare(WordTuple<String, Integer> t1, WordTuple<String, Integer> t2) {
                int x_cmp = t2.word.compareTo(t1.word);
                return x_cmp;
            }
        };

        treeSet = new TreeSet<>(comparator);
    }

    @Override
    public void addWord(String word) {
        WordTuple<String, Integer> foundTuple = null;

        for(WordTuple<String, Integer> node : treeSet)
        {
            if (node.word.equals(word))
            {
                foundTuple = node;
            }
        }

        if (foundTuple != null)
        {
            treeSet.remove(foundTuple);
            treeSet.add(new WordTuple<>(foundTuple.word, foundTuple.count + 1));
        }
        else
            treeSet.add(new WordTuple<>(word, 1));
    }

    @Override
    public void clear() {
        treeSet.clear();
    }

    @Override
    public List<WordTuple<String, Integer>> searchWords(String prefix, int wordCount)
    {
        ArrayList<WordTuple<String, Integer>> list = new ArrayList<>();

        // Ищем слова, начинающиеся с префикса.
        for(WordTuple<String, Integer> node : treeSet)
        {
            if (node.word.startsWith(prefix))
            {
                list.add(node);
            }
        }

        // Сортируем найденные слова.
        list.sort(new WordTupleComparator());

        // Возвращаем только первые wordCount слов.
        if (list.size() <= wordCount)
            return list;
        return list.subList(0, wordCount);
    }
}
