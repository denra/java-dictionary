import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * Created by linux on 21.12.16.
 */
public class UsefulFileWriter {
    private PrintWriter writer;

    public UsefulFileWriter(String fullPathToFile)
    {
        try
        {
            // Создаем записывающую сущность.
            writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fullPathToFile, true)));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            System.exit(-42);
        }
    }

    public void writeLines(String[] lines)
    {
        // Записываем в файл переданные строки.
        for(String line : lines)
        {
            writeLine(line);
        }
    }

    public void writeLine(String line)
    {
        writer.append(line);
        writer.append('\n');
    }

    public void close()
    {
        // Закрываем записывающую сущность.
        writer.close();
    }
}
