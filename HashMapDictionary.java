import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by linux on 21.12.16.
 */
public class HashMapDictionary implements IDictionary {
    HashMap<String, Integer> dict = new HashMap<>();

    @Override
    public void addWord(String word)
    {
        if (dict.containsKey(word))
            dict.put(word, dict.get(word) + 1);
        else
            dict.put(word, 1);
    }

    @Override
    public void clear() {
        dict.clear();
    }

    @Override
    public List<WordTuple<String, Integer>> searchWords(String prefix, int wordCount)
    {
        ArrayList<WordTuple<String, Integer>> list = new ArrayList<>();

        // Ищем слова, начинающиеся с prefix.
        for(String word : dict.keySet())
        {
            if (word.startsWith(prefix))
                list.add(new WordTuple<>(word, dict.get(word)));
        }

        // Сортируем слова.
        list.sort(new WordTupleComparator());

        // Выводим первые wordCount слов.
        if (list.size() <= wordCount)
            return list;

        return list.subList(0, wordCount);
    }
}
