import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by linux on 19.12.16.
 */
public class TestRoom {
    public final static String inputPath = "war-peace.txt";

    static List<String> getFileWords()
    {
        File path = new File(inputPath);
        ArrayList<String> fileWords = new ArrayList<>();

        try
        {
            Scanner scanner = new Scanner(path);

            String[] wordBuffer = null;
            String curLine = null;

            while (scanner.hasNextLine())
            {
                curLine = scanner.nextLine();

                wordBuffer = curLine.split("[^a-zA-Z]");

                for (String curWord : wordBuffer) {
                    if (!curWord.matches(""))
                        fileWords.add(curWord);
                }
            }
            scanner.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            System.exit(-42);
        }

        return fileWords;
    }

    static void buildDictionary(IDictionary dict, int wordCount, List<String> fileWords)
    {
        int curCount = 0;
        for (String word : fileWords)
        {
            if (curCount == wordCount)
                break;

            dict.addWord(word);
            curCount++;
        }
    }

    public static void TestHashMap(HashMapDictionary dict, String prefix)
    {
        List<String> fileWords = getFileWords();


        String buildingPath = "/home/linux/IdeaProjects/FrequencyDictionary/Report/hash_map_building";
        String searchingPath = "/home/linux/IdeaProjects/FrequencyDictionary/Report/hash_map_searching";

        int j = 0;

        long startTime = 0;
        long endTime = 0;
        long[] times = new long[3];

        long sum = 0;
        long average = 0;

        // Сущности записи результатов.
        UsefulFileWriter buildWriter = new UsefulFileWriter(buildingPath);
        UsefulFileWriter searchWriter = new UsefulFileWriter(searchingPath);
        UsefulFileWriter logWriter = new UsefulFileWriter("/home/linux/IdeaProjects/FrequencyDictionary/Report/log");

        /* Проводим 20 тестов с разной наполняемостью словаря */
        for (int i = 1; i <= 20; i++)
        {
            logWriter.writeLine("Current test number: " + Integer.toString(i));

            /* Тестируем скорость построения. */
            // Проводим три построения.
            for (j = 0; j < 3; j++)
            {
                logWriter.writeLine("Current dict_build number: " + Integer.toString(j));

                // Очищаем словарь.
                dict.clear();

                // Проводим тест.
                startTime = System.currentTimeMillis();
                TestRoom.buildDictionary(dict, 36500 * i, fileWords);
                endTime = System.currentTimeMillis();

                logWriter.writeLine("Current word count to building: " + Integer.toString(36500 * i));
                logWriter.writeLine("Current dict size: " + Integer.toString(dict.dict.size()));

                // Запоминаем время выполнения теста.
                times[j] = endTime - startTime;
                logWriter.writeLine("Current dict builing time: " + Long.toString(times[j]));
            }

            // Считаем среднее время построения.
            sum = 0;
            for (j = 0; j < 3; j++)
                sum += times[j];
            average = sum / 3L;
            logWriter.writeLine("Current average builing time: " + Long.toString(average));

            // Записываем результат.
            buildWriter.writeLine(Long.toString(average));


            /* Тестируем скорость поиска. */
            // Проводим три поиска.
            for (j = 0; j < 3; j++)
            {
                // Проводим тест.
                startTime = System.nanoTime();
                dict.searchWords(prefix, 30);
                endTime = System.nanoTime();

                // Запоминаем время выполнения теста.
                times[j] = endTime - startTime;

                logWriter.writeLine("Current dict searching time: " + Long.toString(times[j]));
            }

            // Считаем среднее время поиска.
            sum = 0;
            for (j = 0; j < 3; j++)
                sum += times[j];
            average = sum / 3L;
            logWriter.writeLine("Current average builing time: " + Long.toString(average));
            logWriter.writeLine("------");

            // Записываем результат.
            searchWriter.writeLine(Long.toString(average));


        }

        buildWriter.close();
        searchWriter.close();
    }

    public static void testBuilding(IDictionary dict, List<String> fileWords,
                                    String outputPath)
    {
        long startTime = 0;
        long endTime = 0;
        long[] times = new long[3];

        long sum = 0;
        long average = 0;

        // Сущности записи результатов.
        UsefulFileWriter writer = new UsefulFileWriter(outputPath);

        /* Проводим 20 тестов с разным кол-вом слов в словаре.*/
        int j = 0;
        System.out.println("start 20-loop");
        for (int i = 1; i <= 20; i++)
        {
            System.out.println("Iteration number: " + Integer.toString(i));

            /* Тестируем скорость построения. */
            // Проводим три построения.
            for (j = 0; j < 3; j++)
            {

                // Очищаем словарь.
                dict.clear();

                // Проводим тест.
                System.out.println("start " + j + " iteration test");

                startTime = System.nanoTime();
                TestRoom.buildDictionary(dict, 36500 * i, fileWords);
                endTime = System.nanoTime();

                System.out.println("end " + j + " iteration test");


                // Запоминаем время выполнения теста.
                times[j] = endTime - startTime;
            }

            // Считаем среднее время построения.
            sum = 0;
            for (j = 0; j < 3; j++)
                sum += times[j];
            average = sum / 3L;
            System.out.println("Average time: " + Long.toString(average));

            // Записываем результат.
            writer.writeLine(Long.toString(average));
        }

        // Закрытие сущности записи результатов.
        writer.close();
    }

    public static void testSearching(IDictionary dict, List<String> fileWords,
                                     String outputPath, String prefix)
    {
        long startTime = 0;
        long endTime = 0;
        long[] times = new long[3];

        long sum = 0;
        long average = 0;

        // Сущности записи результатов.
        UsefulFileWriter writer = new UsefulFileWriter(outputPath);

        /* Проводим 20 тестов с разным кол-вом слов в словаре.*/
        int j = 0;
        System.out.println("start 20-loop");
        for (int i = 1; i <= 20; i++)
        {
            /* Тестируем скорость поска. */
            System.out.println("Iteration number: " + Integer.toString(i));
            // Строим словарь.
            dict.clear();
            TestRoom.buildDictionary(dict, 36500 * i, fileWords);

            // Проводим три поиска.
            for (j = 0; j < 3; j++)
            {
                // Проводим тест.
                System.out.println("start " + j + " iteration test");

                startTime = System.nanoTime();
                dict.searchWords(prefix, 30);
                endTime = System.nanoTime();

                System.out.println("end " + j + " iteration test");

                // Запоминаем время выполнения теста.
                times[j] = endTime - startTime;
            }

            // Считаем среднее время поиска.
            sum = 0;
            for (j = 0; j < 3; j++)
                sum += times[j];
            average = sum / 3L;
            System.out.println("Average time: " + Long.toString(average));

            // Записываем результат.
            writer.writeLine(Long.toString(average));
        }

        // Закрытие сущности записи результатов.
        writer.close();
    }

    public static void testDictionary(List<String> words, IDictionary dict, String prefix,
                                      String buildingPath, String searchingPath)
    {
        /*System.out.println("start testBuilding");
        TestRoom.testBuilding(dict, words, buildingPath);
        System.out.println("end testBuilding");

        dict.clear();*/

        System.out.println("start testSearching");
        TestRoom.testSearching(dict, words, searchingPath, prefix);
        System.out.println("end testSearching");
    }
}
