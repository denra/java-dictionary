import java.util.ArrayList;
import java.util.List;

/**
 * Created by linux on 21.12.16.
 */
abstract class ListDictionary implements IDictionary {
    protected List<WordTuple<String, Integer>> wordTupleList;

    protected abstract void constructor();

    ListDictionary() {
        constructor();
    }

    @Override
    public void addWord(String word)
    {
        // Если слово уже в словаре, увеличиваем его кол-во,
        for (WordTuple<String, Integer> wordTuple : wordTupleList)
        {
            if (wordTuple.word.equals(word))
            {
                wordTuple.count += 1;
                return;
            }
        }

        // Иначе добавляем в словарь.
        wordTupleList.add(new WordTuple<>(word, 1));
    }

    @Override
    public List<WordTuple<String, Integer>> searchWords(String prefix, int wordCount)
    {
        ArrayList<WordTuple<String, Integer>> list = new ArrayList<>();

        // Ищем слова, начинающиеся с prefix.
        for (WordTuple<String, Integer> wordTuple : wordTupleList)
        {
            if (wordTuple.word.startsWith(prefix))
                list.add(wordTuple);
        }

        // Сортируем найденные слова.
        list.sort(new WordTupleComparator());

        // Возвращаем только первые wordCount слов.
        if (list.size() <= wordCount)
            return list;
        return list.subList(0, wordCount);

    }

    @Override
    public void clear() {
        wordTupleList.clear();
    }
}
