import java.util.LinkedList;

/**
 * Created by linux on 21.12.16.
 */
public class LinkedListDictionary extends ListDictionary {
    @Override
    protected void constructor() {
        wordTupleList = new LinkedList<>();
    }
}
