import java.util.List;

/**
 * Created by linux on 11.12.16.
 */
public class Main {
    public static void main(String[] args)
    {
        List<String> words = TestRoom.getFileWords();

        // Тестируем HashMapDictionary
        HashMapDictionary hmDict = new HashMapDictionary();
        TestRoom.testDictionary(words, hmDict, "ma", "./Report/hash_map_building",
                                "./Report/hash_map_searching");

        // Тестируем LinkedListDictionary
        LinkedListDictionary llDict = new LinkedListDictionary();
        TestRoom.testDictionary(words, llDict, "ma", "./Report/linked_list_building",
                "./Report/linked_list_searching");

        // Тестируем ArrayListDictionary
        ArrayListDictionary alDict = new ArrayListDictionary();
        TestRoom.testDictionary(words, alDict, "ma", "./Report/array_list_building",
                "./Report/array_list_searching");

        // Тестируем TreeSetDictionary
        TreeSetDictionary tsDict = new TreeSetDictionary();
        TestRoom.testDictionary(words, tsDict, "ma", "./Report/tree_set_building",
                "./Report/tree_set_searching");
    }
}
