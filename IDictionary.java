import java.util.List;

/**
 * Created by linux on 19.12.16.
 */
public interface IDictionary {
    public void addWord(String word);
    public List<WordTuple<String, Integer>> searchWords(String prefix, int wordCount);
    public void clear();
}
